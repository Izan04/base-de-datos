# Unidad C0: Recapitulación

Izan Garrido Quintana.

En este documento se habla sobre las bases de datos, su definicion, caracteristicas y posibilidades. Además de ejemplos a tratar.


## Concepto y origen de las bases de datos

Se encarga de almacenar datos y de conectarlos entre si de manera ordenada, resuelve el problema de almacenar los datos en muchas carpetas y archivos, y los almacena en un solo lugar, además de poder conectarse a la base de datos en red.


## Sistemas de gestión de bases de datos

El sistema de gestión de base de datos (DBMS) es donde se guarda toda la base de datos de la empresa y donde se accede para crear, modificar o consultar datos de forma rápida y ordenada en red o en local. Se gestiona en el WorkBench con MySQL.


### Ejemplos de sistemas de gestión de bases de datos

* Oracle DB
* IBM Db2
* SQLite
* MariaDB
* SQL Server
* PostgreSQL
* MySQL
* MONGO DB
* Microsoft Access

Son software libre: SQLite, MariaDB, PostgreSQL, MySQL y MongoDB. 

Siguen el modelo cliente-servidor: Oracle DB, IBM Db2, MariaDB, SQL Server, PostgreSQL, MySQL, Microsoft Access.

## Modelo cliente-servidor

Se encuentra en un servidor porque ofrece servicios , el servidor tiene un puerto escucha, la relacion es la red, y el cliente hace peticiones y el servidor le envia las respuestas.

El cliente y el servidor pueden ser un hardware o un software

* __Cliente__: Un dispositivo o programa que solicita servicios o recursos de un servidor en una red.

* __Servidor__: Un programa o dispositivo que proporciona servicios o recursos a los clientes en una red.

* __Red__: Una infraestructura que permite la comunicación entre dispositivos electrónicos.

* __Puerto de escucha__: Un número de identificación asociado a un programa o servicio en un servidor que está esperando conexiones de clientes.

* __Petición__: Un mensaje enviado por un cliente a un servidor solicitando algún tipo de acción o recurso.

* __Respuesta__: Un mensaje enviado por un servidor a un cliente en respuesta a una petición, proporcionando datos, servicios o recursos solicitados.

## SQL

Es un lenguaje standard desde los años 80 que permite hacer consultas a bases de datos en forma ordenada

### Instrucciones de SQL

#### DDL ( Data Definition Language)

Definir como van a ser los datos: CREATE, ALTER, DROP...

#### DML (DATA MANIPULATION LANGUAGE)

Permite llevar a cabo las tareas de consulta o modificiación de datos: UPDATE, SELECT, INSERT...

#### DCL (DATA CONTROL LANGUAGE)

Permite al administrador controlar el acceso a los datos contenidos en la Base de Datos: GRANT, REMOVE

#### TCL (TRANSACTION CONTROL LAGUAGE)

Se utiliza para controlar el procesamiento de transacciones en una base de datos: COMMIT, ROLLBACK.

## Bases de datos relacionales

Las bases de datos relacionales es una forma de estructurar información en tablas filas y columnas y se relacionan mediante una clave


* __Relación (tabla)__: Una estructura de datos organizada en filas y columnas que representa una colección de entidades y sus relaciones en una base de datos relacional.

* __Atributo/Campo (columna)__: Una característica o propiedad específica de los datos que se almacena en una tabla, representada por una columna.

* __Registro/Tupla (fila)__: Una instancia individual de datos en una tabla que contiene valores para cada uno de los atributos/campos definidos, representada por una fila.

## Referencias
* https://es.wikipedia.org/wiki/Base_de_datos 
* http://www.tierradelazaro.com