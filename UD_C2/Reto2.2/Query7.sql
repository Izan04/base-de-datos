USE Chinook;

SELECT t.Name, t.UnitPrice, A.Title FROM Track AS t
JOIN Album AS a
ON T.AlbumId = A.AlbumId
ORDER BY t.UnitPrice DESC, A.AlbumId
