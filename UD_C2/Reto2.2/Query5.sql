USE Chinook;

SELECT
	E2.FirstName AS Empleado,
    E1.FirstName AS Subordinado
FROM Employee AS E1
JOIN Employee AS E2
	ON E1.EmployeeId = E2.ReportsTo;