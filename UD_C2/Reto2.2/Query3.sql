USE Chinook;

SELECT E.Firstname AS Nombre, 
E.LastName AS Apellido
FROM Employee AS E
WHERE EXISTS(
	SELECT * FROM Customer AS C 
	WHERE E.EmployeeId = C.SupportRepId
);