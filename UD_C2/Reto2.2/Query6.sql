USE Chinook;

SELECT t.Name
FROM Track AS t
WHERE t.TrackId IN (

	SELECT il.TrackId 
	FROM InvoiceLine AS il
	WHERE il.InvoiceId in (
    
		SELECT i.InvoiceId
		FROM Invoice AS i
		WHERE i.CustomerId = (
        
			SELECT c.CustomerId
            FROM Customer AS c
            WHERE c.FirstName = 'Luis'
			AND c.LastName = 'Rojas'
		)	
	)
);
