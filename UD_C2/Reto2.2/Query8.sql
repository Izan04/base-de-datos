USE Chinook;

SELECT FirstName, LastName FROM Customer
WHERE CustomerID IN (
	SELECT CustomerId 
    FROM Invoice
    WHERE InvoiceId IN (
		SELECT InvoiceId
        FROM InvoiceLine
        WHERE TrackId IN (
			SELECT TrackId
            FROM Track
            WHERE AlbumId IN (
				SELECT AlbumID
                FROM Album
                WHERE ArtistId = 51
            )
        )
    )
)