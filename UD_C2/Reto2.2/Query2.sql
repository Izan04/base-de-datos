USE Chinook;
/*
SELECT ar.Name AS 'Artista',
t.Milliseconds/60000 AS 'Duración en minutos'
FROM Artist AS ar
JOIN Album AS al
JOIN Track AS t
ON ar.ArtistId = al.ArtistId
AND al.AlbumId = t.AlbumId
WHERE t.Milliseconds/60000 > 5;
*/
SELECT AR.Name, T.Milliseconds
FROM Artist AS AR
JOIN ALBUM AS AL
ON AR.ArtistId = AL.ArtistId
JOIN Track AS T
ON AL.AlbumId = T.AlbumId
WHERE T.Milliseconds IN (
	SELECT T.Milliseconds/60000 
    FROM Track AS T
    WHERE T.Milliseconds/60000 > 5
)