# UNIDAD 2

Izan Garrido.

El código fuente de de las siguientes consultas, se puede consultar en: https://gitlab.com/Izan04/base-de-datos.git

# Reto 1:

En este reto trabajaremos mas a fondo con la secuencia `JOIN`, además de utilizar las funciones de agregación. Para realizar las siguientes 20 querys. Hemos utilizado la base de datos de `Chinook`, y resolveremos las siguientes cuestiones.

# Consultas sobre una tabla

## Query 1
Para encontrar todos los clientes de Francia, seleccionaremos todo con `*` de la tabla `Customer`, y para saber que solo seleccionaremos los clientes que sean de francia, utilizaremos la función `WHERE` de `Country`, y utilizaremos la función `LIKE` sobre "France".


```sql
USE Chinook;

SELECT * FROM Customer
WHERE Country Like "France";
```

## Query 2
Para mostrar las facturas del primer trimeste de este año, seleccionaré todo de la tabla  `Invoice` mientras que el número del mes se menor o igual que 3 con, `MONTH(InvoiceDate) <= 3`, y que el año sea el año actual con `YEAR(CURRENT_DATE())`.


```sql
USE Chinook;

SELECT * FROM Invoice
WHERE MONTH(InvoiceDate) <= 3 
AND YEAR(InvoiceDate) = YEAR(current_date());
```

## Query 3
Para mostrar todas las canciones compuestas por el grupo de música AC/DC, seleccionaré todo de la tabla `Track`, mientras que la columna `Composer`, sea AC/DC, con la función `LIKE AC/DC`.


```sql
USE Chinook;

SELECT * FROM Track
WHERE Composer LIKE "AC/DC";
```

## Query 4
Para mostrar las 10 canciones que mas tamaño ocupan, seleccionaré todo de la tabla 'Track', y lo ordenaré por tamaño, con la columna `Bytes` de forma descendente, y para mostrar solo las primeras 10, seleccionaré las función `LIMIT`, y lo estableceré en 10.


```sql
USE Chinook;

SELECT * from Track
ORDER BY Bytes Desc
LIMIT 10;
```

## Query 5
Para mostrar el nombre de los países en los que tenemos clientes, seleccionaré los diferentes paises, para que no se puedan repetir, con la función `DISTINCT(Country)`, de la tabla `Customer`, mientras que el nombre no esté vacio, seleccionando la columna `FirstName`, con `IS NOT NULL`.


```sql
USE Chinook;

SELECT DISTINCT(Country) 
FROM Customer
WHERE FirstName IS NOT NULL;
```

## Query 6
Para mostrar todos los generos musicales, seleccionaré los diferentes generos con la función `DISTINCT(Name)` de la tabla `Genre`, que se corresponde al género.


```sql
USE Chinook;

SELECT DISTINCT(Name) 
FROM Genre;
```

# Consultas sobre múltiples tablas

## Query 7
Para mostrar todos los artistas junto a sus álbumes, seleccionaré `Artist.Name` y `Album.Title`, que se corresponden al nombre del artista, y el nombre del Álbum, de la tabla `Artist`, y haré un `JOIN` de la tabla `Album`, donde `Artist.ArtistId` sea igual a `Album.AlbumId`.

```sql
USE Chinook;

SELECT Artist.Name,
	Album.Title
FROM Artist 
JOIN Album
	ON Artist.ArtistId = Album.AlbumId;
```

## Query 8
Para mostrar los nombres de los 5 empleados más jóvenes junto a los nombres de sus supervisores, haré un `SELECT` de la fecha de nacimiento del empleado con `E1.BirthDate`, además de seleccionar el nombre del empleado, y el nombre del supervisor, con `E1.FirstName AS 'Empleado'` y `E2.FirstName AS 'Supervisor'`, de la tabla `Employee AS E1`, y haré un `JOIN` de la misma tabla, ya que los supervisores también son empleados, con `JOIN Employee AS E2`, cuando `E1.EmployeeId` sea igual a 'E2.ReportstTo',  lo ordenaré por fecha de nacimiento de forma descendente usando `ORDER BY E1.BirthDate DESC`, y le establecré el límite en 5, para mostrar solo los 5 más jovenes con `Limit 5`.

```sql
USE Chinook;

SELECT E1.BirthDate,
    E1.FirstName AS Empleado,
    E2.FirstName AS Supervisor
FROM Employee AS E1
JOIN Employee AS E2
	ON E1.EmployeeId = E2.ReportsTo
ORDER BY E1.BirthDate DESC
LIMIT 5;
```

## Query 9
Para mostrar todas las facturas de los clientes berlineses. Y mostrarse las columnas:
fecha de la factura, nombre completo del cliente, dirección de facturación,
código postal, país e importe. Seleccionaré fecha de factura con `i.InvoiceDate AS 'Fecha factura'`, el nombre completo del cliente con `c.FirstName AS 'Nombre'` y `c.LastName AS 'Apellido'`, la dirección de facturación con `i.BillingAddress AS 'Dirección de facturación'`, el código postal con `c.PostalCode AS 'Código postal'`, el país con `c.Country AS 'Pais'` y el importe con `i.Total AS 'Importe'`, de la tabla `Invoice AS i`, haciendo un `Join` a la tabla `Customer AS c`, cuando el `i.CustomerId` sea igual a `c.CustomerId`. 


```sql
USE Chinook;

SELECT i.InvoiceDate AS 'Fecha factura',
	c.FirstName AS 'Nombre',
    c.LastName AS 'Apellido',
    i.BillingAddress AS 'Dirección de facturación',
    c.PostalCode AS 'Código postal',
    c.Country AS 'Pais',
    i.Total AS 'Importe'
FROM Invoice AS i
JOIN Customer AS c
	ON i.CustomerId = c.CustomerId;
```

## Query 10
Para mostrar las listas de reproducción cuyo nombre comienza por C, junto a todas
sus canciones, ordenadas por álbum y por duración, seleccionaré las columnas `A.Title AS Album`, `PL.Name AS PlayList`, `T.Name AS Canción`, `T.Milliseconds AS Milisegundos`, de la tabla `Playlist AS PL`, y haré 3 `JOIN`, uno para la tabla `PlaylistTrack AS PLT`, otro para `Track AS T`, y otro para `Album AS A` donde la columna `PL.PlaylistId` sea igual a `PLT.PlaylistId`, la columna `T.TrackId` sea igual a `PLT.TrackId` y la columna `A.AlbumId` sea igual a `T.AlbumId`, y para los nombres que comiencen por la letra 'C', haremos un `WHERE` para la columna `PL.Name` y usaremos la función `LIKE "C%"`, y lo ordenaré por album y duración con `ORDER BY` de las columnas `T.Milliseconds` y `A.Title`. 


```sql
USE Chinook;

SELECT A.Title AS Album, 
	PL.Name AS PlayList, 
	T.Name AS Canción,
    T.Milliseconds AS Milisegundos
FROM Playlist AS PL
JOIN PlaylistTrack AS PLT
JOIN Track AS T
JOIN Album AS A
	ON PL.PlaylistId = PLT.PlaylistId
	AND T.TrackId = PLT.TrackId
    AND A.Albumid = T.Albumid
WHERE PL.Name LIKE "C%"
ORDER BY T.Milliseconds, A.Title;
```

## Query 11
Para mostrar qué clientes han realizado compras por valores superiores a 10€, ordenados
por apellido, seleccionaré la columna `c.FirstName AS 'Nombre'`, también la columna `c.LastName AS 'Apellido'`, y la columna `i.Total AS 'Compra'`,ya que estas muestran el nombre completo y el valor total de la compra, además haré un `SELECT` de la tabla `Customer AS c`, y haré un `JOIN` para la tabla `Invoice AS i`, donde `c.CustomerId` sea igual a `i.CustomerId`, mientras que el total de la compra `i.Total` sea mayor que 10, y lo ordenare por apellido con `ORDER BY` respectivamente de la tabla `c.LastName`.


```sql
USE Chinook;

SELECT c.FirstName AS Nombre,
	c.LastName AS Apellido,
	i.Total AS Compra
FROM Customer AS c
JOIN Invoice AS i
ON c.CustomerId = i.CustomerId
WHERE i.Total > 10
ORDER BY c.LastName;
```

# Consultas con funciones de agregación

## Query 12
Para mostrar el importe medio, mínimo y máximo de cada factura, Seleccionaré tres veces la columna `Total` de la tabla `Invoice`, pero usando funciones de agregación `MIN(), MAX() y AVG()`.


```sql
USE Chinook;

SELECT MIN(Total) AS 'Importe Minimo',
	MAX(Total) AS 'Importe Maximo',
	AVG(Total) AS 'Importe Medio'
FROM Invoice;
```

## Query 13
Para mostrar el número total de artistas seleccionaré la columna `Name`, respectivamente de la tabla `Artist`, usando la función de agregación `COUNT()`, que cuenta el número de veces que se repite una fila.


```sql
USE Chinook;

SELECT COUNT(Name) AS 'Número de artistas' 
FROM Artist;
```

## Query 14
Para mostrar el número de canciones del álbum 'Out Of Time' seleccionaré la columna `t.Name`, usando la función de `COUNT()` en esta, de la tabla `Album AS a`, haciendo un `JOIN` de la tabla `Track AS t`, donde `a.AlbumId` sea igual a `t.AlbumId`, mientras que la columna `a.Title` se llame 'Out Of Time' usando la función `LIKE`.


```sql
USE Chinook;

SELECT COUNT(t.Name) AS 'NÚmero de canciones'
FROM Album AS a
JOIN Track AS t
	ON a.AlbumId = t.AlbumId
WHERE a.Title LIKE 'Out Of Time';
```

## Query 15
Para mostrar el número de paises donde tenemos clientes seleccionaré la columna `Country`, respectivamente de la tabla `Customer`, usando la función de agregación `COUNT()`, además de usar de entro de esta la función `DISTINCT()`, para que no se puedan repetir los países, de esta manera `COUNT(DISTINCT(Country))`, mienstra que la columna del nombre no esté vaciá, con `FirstName IS NOT NULL`.


```sql
USE Chinook;

SELECT COUNT(DISTINCT(Country)) AS 'Número de paises'
FROM Customer
WHERE FirstName IS NOT NULL;
```

## Query 16
Para mostrar el número de canciones de cada género, seleccionaré la columna `g.Name` dos veces, pero en una de ellas usando la función de agregación `COUNT()`, usando la tabla `Track AS t`, y haciendo un `JOIN` de la tabla `Genre AS g`, donde la columna `t.GenreId` sea igual a la columna `g.GenreId`, agrupado por nombre, con la función `GROUP BY g.Name`.


```sql
USE Chinook;

SELECT  g.Name AS 'GENERO',
	COUNT(g.Name) AS 'Número de canciones'
FROM Track AS t
JOIN Genre AS g
	ON t.GenreId = g.GenreId
GROUP BY g.Name
```

## Query 17
Para mostar los álbumes ordenados por el número de canciones que tiene cada uno, seleccionaré la columna `a.Title`, y la columna `t.Name`, usando en esta la función de agregación `COUNT()`, de la tabla `Album AS a`, además haciendo un `JOIN` de la tabla `Track AS t`, donde la columna `a.AlbumId` sea igual a la columna `t.AlbumId`, agrupado por el nombre del Album con `GROUP BY a.Title`, ordenado por el número de canciones de manera descendente con `ORDE BY COUNT(t.Name) DESC`.


```sql
USE Chinook;

SELECT a.Title AS 'Album',
	COUNT(t.Name) AS 'Número de canciones'
FROM Album AS a
JOIN Track AS t
	ON a.AlbumId = t.AlbumId
GROUP BY a.Title
ORDER BY COUNT(t.Name) DESC;
```

## Query 18
Para encontrar los géneros musicales más populares, seleccionaré la columna `G.Name`, y seleccionaré la función de agregación `COUNT(*)`, para contar las veces que se ha comprado cada género, de la tabla `Genre AS G`, usando dos veces la función `JOIN` para seleccionar las tablas `Track AS T` y `InvoiceLine AS IL`, donde la columna `G.GenreId` sea igual a la columna `T.GenreId`, y la columna `T.TrackId` sea igual a `IL.TrackId`, agrupado por el nombre del género con `GROUP BY G.Name`, y ordenado por el número de veces comprado con la función `ORDER BY COUNT(*) DESC` de manera descendente.


```sql
USE Chinook;

SELECT G.Name AS 'Genero',
	COUNT(*) AS 'Veces comprado'
FROM Genre AS G
JOIN Track AS T
JOIN InvoiceLine AS IL
	ON G.GenreId = T.GenreId
	AND T.TrackId = IL.TrackId
GROUP BY G.Name
ORDER BY COUNT(*) DESC
```

## Query 19
Para listar los 6 álbumes que acumulan más compras, seleccionaré la columna `A.Title`, y la función de agregación `COUNT(*)` para seleccionar las veces que se ha comprado, de la tabla `Album AS A`, haciendo 2 veces `JOIN`, de las tablas `Track AS T`, y `InvoiceLine AS IL`, donde la columna `A.AlbumId` se igual a `T.AlbumId` y la columna `T.TrackId` sea igual a `IL.TrackId`, agrupado por el nombre del album con `GROUP BY Title`, y ordenado por el número de veces comprado de forma descendente con la función de agregación `ORDER BY COUNT(*) DESC`, para estableces el límite en 6 con `LIMIT 6`. 


```sql
USE Chinook;

SELECT A.Title AS 'Album',
	COUNT(*) AS 'Veces Comprado' 
FROM Album AS A
JOIN Track AS T
JOIN InvoiceLine AS IL
	ON A.AlbumId = T.AlbumId
    AND T.TrackId = IL.TrackId
GROUP BY Title
ORDER BY COUNT(*) DESC
LIMIT 6
```

## Query 20
Para mostar los países en los que tenemos al menos 5 clientes, seleccionaremos la columna `Country` para el nombre del Pais, y un contador para el número de clientes con la función de agregación `COUNT(*)`, de la tabla `Customer`, agrupado por País cont `GROUP BY Country`, mientras que el número de clientes sea mayor que 4 con la función de agregación `HAVING COUNT(*) > 4`, ordenado por el número de clientes de manera descendente con la función `ORDER BY COUNT(*) DESC`.


```sql
USE Chinook;

SELECT Country AS 'Pais', COUNT(*) AS 'Número de clientes'
FROM Customer
GROUP BY Country
HAVING COUNT(*) > 4
ORDER BY COUNT(*) DESC;
```