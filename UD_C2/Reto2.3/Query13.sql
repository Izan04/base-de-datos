USE Chinook;

SELECT a.AlbumId,
    a.Title AS Album,
    COUNT(t.TrackId) AS N_Canciones
FROM Album a
JOIN Track t 
ON a.AlbumId = t.AlbumId
GROUP BY a.AlbumId, a.Title
ORDER BY a.AlbumId;