# Reto 2.3: Consultas con subconsultas II
**[Bases de Datos] Unidad Didáctica 2: DML - Consultas Avanzadas**

Para este reto, volveremos a usar la base de datos Chinook (más información en el Reto 2.1).

El codigo fuente de estas consultas se encuentra en https://gitlab.com/Izan04/base-de-datos.git.

![Diagrama relacional de Chinook (fuente: github.com/lerocha/chinook-database).](https://github.com/lerocha/chinook-database/assets/135025/cea7a05a-5c36-40cd-84c7-488307a123f4)


## Subconsultas Escalares (Scalar Subqueries)

### Consulta 1
Para obtener las canciones con una duración superior a la media seleccionaré el id de canción, el nombre de la canción y los milisegundos entre 1000 para pasarlo a segundos de la tabla Track, mientras q los milisegundos sea mayor que la media, que lo haré con otro select de la media de milisegeundos con AVG(Milliseconds) de la tabla track, y lo ordenare por milisegundos de forma ascendente

```sql

USE Chinook;

SELECT TrackId AS ID, 
Name AS CANCIÓN, 
Milliseconds/1000 AS SEGUNDOS 
FROM Track
WHERE Milliseconds > (SELECT AVG(Milliseconds) FROM TRACK)
ORDER BY MILLISECONDS ASC;

```

### Consulta 2
Para listar las 5 últimas facturas del cliente cuyo email es "emma_jones@hotmail.com" seleccionaré todo de la tabla Invoice mientras que el id del cliente esté en la subconsulta que selecciona el id del cliente de la tabla customer, mientras que el email sea "emma_jones@hotmail.com", y lo ordenaré por el id de factura de forma descendente y le pondré el límite en 5.

```sql

USE Chinook;
	SELECT * FROM Invoice
    WHERE CustomerId IN (
		SELECT CustomerId FROM Customer
        WHERE Email LIKE 'emma_jones@hotmail.com'
    )

ORDER BY InvoiceId DESC
LIMIT 5

```

## Subconsultas de varias filas

### Consulta 3
Para mostrar las listas de reproducción en las que hay canciones de reggae seleccionaré todo de la tabla playlist mientra que el id esté en otra subconsulta que selecciona el id de la playlist de la tabla playlistrack mientras que trackid esté en otra consulta que selecciona el trackid de la tabla track mientras que el id del genero este en otro select del id genero de la tabla genre mientras que el nombre de la canción sea reggae.

```sql

USE Chinook;

SELECT * FROM PlayList
WHERE PlayListId IN (
	SELECT PlayListId FROM PlayListTrack
    WHERE TrackId IN (
		SELECT TrackId FROM Track 
        WHERE GenreId IN(
			SELECT GenreId FROM Genre
            WHERE Name LIKE 'reggae'
        )
	)
);

```

### Consulta 4
Para obtener la información de los clientes que han realizado compras superiores a 20€ seleccionaré todo de la tabla customer mientras que el idcustomer este en un select del idcustomer de la tabla invoice mientras que el dinero total sea mas que 20.

```sql

USE Chinook;

SELECT * FROM Customer
WHERE CustomerId IN (
	SELECT CustomerId FROM Invoice
    WHERE Total > 20
);


```

### Consulta 5
Para obtener los álbumes que tienen más de 15 canciones, junto a su artista seleccionaré todo de la tabla album y haré un join de la tabla artist mientras que el id del album esté en un select de el id del album de la tabla track agrupado por el id del album teniendo que el count del albumid sea mayor que 15

```sql

USE Chinook;

SELECT * FROM Album AS AL
JOIN Artist AS AR
ON AL.ArtistId = AR.ArtistId
WHERE AlbumId IN (
	SELECT T.AlbumId FROM Track AS T
    GROUP BY AlbumId
    HAVING COUNT(AlbumId) > 15
    
)

```

### Consulta 6
Para obtener los álbumes con un número de canciones superiores a la media seleccionaré todo de la tabla album mientras que el id del album esté en un select de el id del album de la tabla track, agrupado por el albumid teniendo que el count de todo sea mayor que un select de media de N_Canciones de un select de albumid y de un count de todo AS N_Canciones, de la tabla track agrupado por el id del album, todo esto llamado Album_NCanciones, porque tiene que tener algun nombre porque si no de error.

```sql
USE Chinook;    

SELECT * FROM Album
    WHERE AlbumId IN (
    SELECT AlbumId FROM Track
    GROUP BY AlbumId
    HAVING COUNT(*) > (
        SELECT AVG(N_Canciones) FROM(
            SELECT AlbumId, COUNT(*) AS N_Canciones
            FROM Track
            GROUP BY AlbumId
        ) AS Album_NCanciones
    )
)


```

### Consulta 7
Para obtener los álbumes con una duración total superior a la media seleccionaré el título del album y la suma de los milisegundos de la tabla album, hago un join a la tabla track, lo agrupo por id del album y titulo del album, teniendo que la suma de los miliesgundos sea mayor que un select de la media de la suma de milisegundos de otro select, que selecciona la suma de los milisegundos de la tabla track, agrupado por albumid .

```sql

USE Chinook;

SELECT Album.Title AS Album,
SUM(T.Milliseconds) AS Duración
FROM Album
JOIN Track AS T
ON Album.AlbumId = T.AlbumId
GROUP BY Album.AlbumId, Album.Title
HAVING SUM(T.Milliseconds) > (
    SELECT AVG(Duración) 
    FROM (
        SELECT SUM(Milliseconds) AS Duración 
        FROM Track 
        GROUP BY AlbumId) AS duracionalbum);


```

### Consulta 8
Para obtener las canciones del género con más canciones seleccionaré el nombre de la tabla track mientras q el id del genero sea igual a un select del id del genero de la tabla track, agrupado por id de genero, ordenado por un count del id de genero descendente y con limite en 1.

```sql

USE Chinook;

SELECT Name
FROM Track
WHERE GenreId = (
    SELECT GenreId 
    FROM Track
    GROUP BY GenreId
    ORDER BY COUNT(GenreId) DESC
    LIMIT 1
);


```

### Consulta 9
Para obtener las canciones de la playlist con más canciones seleccionaré el nombre de la tabla Track, y haré un join a la tabla PlaylistTrack mientras que el id de la playlist sea igual a un select del id de playlist de la tabla track haciendo un join a la tabla PlaylistTrack agrupado por el id de la playlist ordenado por un count del id de playlist decendente, con limite en 1.

```sql

USE Chinook;

SELECT Name FROM Track AS T
JOIN PlaylistTrack AS PLT
ON T.TrackId = PLT.TrackId
WHERE PLT.PlaylistId = (
SELECT PLT.PlaylistId FROM Track AS T
JOIN PlaylistTrack AS PLT
ON T.TrackId = PLT.TrackId
GROUP BY PLT.PlaylistId
ORDER BY COUNT(PLT.PlaylistId) DESC
LIMIT 1
)

```


## Subconsultas Correlacionadas (Correlated Subqueries):

### Consulta 10
para mostrar los clientes junto con la cantidad total de dinero gastado por cada uno en compras seleccionaré el nombre y la suma total de la tabla customer, con un join a la tabla invoice, agrupado por nombre y ordenado por nombre.

```sql

USE Chinook;

SELECT FirstName, SUM(Total) AS Total
FROM Customer AS C
JOIN Invoice AS I
ON C.CustomerId = I.CustomerId
GROUP BY FirstName
ORDER BY FirstName;



```

### Consulta 11
Para obtener empleados y el número de clientes a los que sirve cada uno de ellos seleccionarñe el supportrepid y el contador de todo de la tabla cusotmer agrupado por supportrepid.

```sql

USE Chinook;

SELECT SupportRepId, COUNT(*) AS N_Customers
FROM Customer
GROUP BY SupportRepId

```

### Consulta 12
Ventas totales de cada empleado.
Esta consulta no he sabido realizarla
```sql



```

### Consulta 13
Para obtener los álbumes junto al número de canciones en cada uno, seleccionaré el id del album, el titulo y el count del id track, de la tabla album, con un join a la tabla track, agrupado por albumid, titulo y ordenado por albumid.

```sql

USE Chinook;

SELECT a.AlbumId,
    a.Title AS Album,
    COUNT(t.TrackId) AS N_Canciones
FROM Album a
JOIN Track t 
ON a.AlbumId = t.AlbumId
GROUP BY a.AlbumId, a.Title
ORDER BY a.AlbumId;

```

### Consulta 14
Para obtener el nombre del álbum más reciente de cada artista seleccionaré el nombre de la tabla artist y el titulo de la tabla album, de un select de el id del artista, el max del id del album para obtener el album mas reciente de la tabla album, agrupado por id de artist, todo llamado como "a", y hago un join a la tabla artist.

```sql

SELECT ar.Name, al.Title
FROM (
SELECT ArtistId, MAX(AlbumId) AS recienteAlbumId
FROM Album
    GROUP BY ArtistId
    ) AS a
JOIN Album AS al 
ON a.recienteAlbumId = al.AlbumId
JOIN Artist AS ar 
ON a.ArtistId = ar.ArtistId;


```


## Referencias
- [1] https://learnsql.es/blog/subconsulta-correlacionada-en-sql-una-guia-para-principiantes/
- [2] https://learnsql.es/blog/cuales-son-los-diferentes-tipos-de-subconsultas-sql/