SELECT ar.Name, al.Title
FROM (
SELECT ArtistId, MAX(AlbumId) AS recienteAlbumId
FROM Album
    GROUP BY ArtistId
    ) AS a
JOIN Album AS al 
ON a.recienteAlbumId = al.AlbumId
JOIN Artist AS ar 
ON a.ArtistId = ar.ArtistId;
