USE Chinook;

SELECT * FROM Customer
WHERE CustomerId IN (
	SELECT CustomerId FROM Invoice
    WHERE Total > 20
);
