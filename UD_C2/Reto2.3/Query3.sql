USE Chinook;

SELECT * FROM PlayList
WHERE PlayListId IN (
	SELECT PlayListId FROM PlayListTrack
    WHERE TrackId IN (
		SELECT TrackId FROM Track 
        WHERE GenreId IN(
			SELECT GenreId FROM Genre
            WHERE Name LIKE 'reggae'
        )
	)
);