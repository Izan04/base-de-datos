USE Chinook;

SELECT FirstName, SUM(Total) AS Total
FROM Customer AS C
JOIN Invoice AS I
ON C.CustomerId = I.CustomerId
GROUP BY FirstName
ORDER BY FirstName;

