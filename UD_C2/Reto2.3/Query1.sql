USE Chinook;

SELECT TrackId AS ID, 
Name AS CANCIÓN, 
Milliseconds/1000 AS SEGUNDOS 
FROM Track
WHERE Milliseconds > (SELECT AVG(Milliseconds) FROM TRACK)
ORDER BY MILLISECONDS ASC;