USE Chinook;

SELECT Name
FROM Track
WHERE GenreId = (
    SELECT GenreId 
    FROM Track
    GROUP BY GenreId
    ORDER BY COUNT(GenreId) DESC
    LIMIT 1
);
