USE Chinook;

SELECT * FROM Album AS AL
JOIN Artist AS AR
ON AL.ArtistId = AR.ArtistId
WHERE AlbumId IN (
	SELECT T.AlbumId FROM Track AS T
    GROUP BY AlbumId
    HAVING COUNT(AlbumId) > 15
    
)