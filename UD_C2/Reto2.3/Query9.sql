USE Chinook;

SELECT Name FROM Track AS T
JOIN PlaylistTrack AS PLT
ON T.TrackId = PLT.TrackId
WHERE PLT.PlaylistId = (
SELECT PLT.PlaylistId FROM Track AS T
JOIN PlaylistTrack AS PLT
ON T.TrackId = PLT.TrackId
GROUP BY PLT.PlaylistId
ORDER BY COUNT(PLT.PlaylistId) DESC
LIMIT 1
)