USE Chinook;

SELECT Album.Title AS Album,
SUM(T.Milliseconds) AS Duración
FROM Album
JOIN Track AS T
ON Album.AlbumId = T.AlbumId
GROUP BY Album.AlbumId, Album.Title
HAVING SUM(T.Milliseconds) > (
    SELECT AVG(Duración) 
    FROM (
        SELECT SUM(Milliseconds) AS Duración 
        FROM Track 
        GROUP BY AlbumId) AS duracionalbum);
