USE Chinook;

SELECT c.FirstName AS Nombre,
	c.LastName AS Apellido,
	i.Total AS Compra
FROM Customer AS c
JOIN Invoice AS i
ON c.CustomerId = i.CustomerId
WHERE i.Total > 10
ORDER BY c.LastName;