USE Chinook;

SELECT Country AS 'Pais',
	COUNT(*) AS 'Número de clientes'
FROM Customer
GROUP BY Country
HAVING COUNT(*) > 4
ORDER BY COUNT(*) DESC;