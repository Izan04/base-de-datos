USE Chinook;

SELECT MIN(Total) AS 'Importe Minimo',
	MAX(Total) AS 'Importe Maximo',
	AVG(Total) AS 'Importe Medio'
FROM Invoice;