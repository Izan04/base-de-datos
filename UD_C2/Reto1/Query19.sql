USE Chinook;

SELECT A.Title AS 'Album',
	COUNT(*) AS 'Veces Comprado' 
FROM Album AS A
JOIN Track AS T
JOIN InvoiceLine AS IL
	ON A.AlbumId = T.AlbumId
    AND T.TrackId = IL.TrackId
GROUP BY Title
ORDER BY COUNT(*) DESC
LIMIT 6