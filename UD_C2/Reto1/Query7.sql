USE Chinook;

SELECT Artist.Name,
	Album.Title
FROM Artist 
JOIN Album
	ON Artist.ArtistId = Album.AlbumId;