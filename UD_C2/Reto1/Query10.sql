USE Chinook;

SELECT A.Title AS Album, 
	PL.Name AS PlayList, 
	T.Name AS Canción,
    T.Milliseconds AS Milisegundos
FROM Playlist AS PL
JOIN PlaylistTrack AS PLT
JOIN Track AS T
JOIN Album AS A
	ON PL.PlaylistId = PLT.PlaylistId
	AND T.TrackId = PLT.TrackId
    AND A.Albumid = T.Albumid
WHERE PL.Name LIKE "C%"
ORDER BY T.Milliseconds, A.Title;