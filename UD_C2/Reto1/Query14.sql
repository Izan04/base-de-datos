USE Chinook;

SELECT COUNT(t.Name) AS 'NÚmero de canciones'
FROM Album AS a
JOIN Track AS t
	ON a.AlbumId = t.AlbumId
WHERE A.Title LIKE 'Out Of Time';