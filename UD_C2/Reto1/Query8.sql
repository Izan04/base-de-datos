USE Chinook;

SELECT E1.BirthDate,
	E1.FirstName AS Empleado,
    E2.FirstName AS Supervisor 
FROM Employee AS E1
JOIN Employee AS E2
	ON E1.EmployeeId = E2.ReportsTo
ORDER BY E1.BirthDate DESC
LIMIT 5;