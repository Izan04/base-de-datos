USE Chinook;

SELECT i.InvoiceDate AS 'Fecha factura',
	c.FirstName AS 'Nombre',
    c.LastName AS 'Apellido',
    i.BillingAddress AS 'Dirección de facturación',
    c.PostalCode AS 'Código postal',
    c.Country AS 'Pais',
    i.Total AS 'Importe'
FROM Invoice AS i
JOIN Customer AS c
	ON i.CustomerId = c.CustomerId;