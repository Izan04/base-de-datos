USE Chinook;

SELECT G.Name AS 'Genero',
	COUNT(*) AS 'Veces comprado'
FROM Genre AS G
JOIN Track AS T
JOIN InvoiceLine AS IL
	ON G.GenreId = T.GenreId
	AND T.TrackId = IL.TrackId
GROUP BY G.Name
ORDER BY COUNT(*) DESC