USE Chinook;

SELECT a.Title AS 'Album',
	COUNT(t.Name) AS 'Número de canciones'
FROM Album AS a
JOIN Track AS t
	ON a.AlbumId = t.AlbumId
GROUP BY a.Title
ORDER BY COUNT(t.Name) DESC;