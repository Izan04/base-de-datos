USE Chinook;

SELECT  g.Name AS 'GENERO',
	COUNT(g.Name) AS 'Número de canciones'
FROM Track AS t
JOIN Genre AS g
	ON t.GenreId = g.GenreId
GROUP BY g.Name