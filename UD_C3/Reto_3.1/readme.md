# Reto 3.1 DDL - Definición de datos en MySQL

Para trabajar con una base de datos en MySQL, es esencial conocer algunos comandos básicos para la conexión y exploración de la base de datos:

- **Mostrar bases de datos**: `SHOW DATABASES;`
  - Este comando lista todas las bases de datos disponibles en el servidor MySQL.
- **Mostrar tablas**: `SHOW TABLES;`
  - Después de seleccionar una base de datos con `USE Rallané;`, este comando lista todas las tablas dentro de esa base de datos.
- **Mostrar columnas**: `SHOW COLUMNS FROM Pasajeros;`
  - Este comando muestra la estructura de una tabla específica, incluyendo los nombres de las columnas y sus tipos de datos.

# TCL (Transaction Control Language) y Control de Transacciones

- **AUTOCOMMIT**:
  - Por defecto, MySQL opera en modo `AUTOCOMMIT=1`, lo que significa que cada declaración SQL se considera una transacción individual y se confirma automáticamente.
  - Para desactivar el autocommit y manejar transacciones manualmente, se utiliza `SET autocommit=0;`.

- **COMMIT**:
  - `COMMIT;` se usa para confirmar todas las operaciones de una transacción, haciendo permanentes los cambios en la base de datos. 

- **ROLLBACK**:
  - `ROLLBACK;` se utiliza para deshacer todas las operaciones realizadas en la transacción actual, revirtiendo la base de datos al estado anterior al inicio de la transacción.

# Creación de Bases de Datos y Tablas

- **Tipos de Datos**:
  - **INT**: Para campos que almacenan números enteros.
  - **VARCHAR(tamanyo)**: Para cadenas de texto de longitud variable.
  - **DATE**: Para almacenar fechas.
  
- **Restricciones**:
   - **PK:** Primary key (Clave primaria).
   - **NN:** Not Null (No nulo).
   - **UQ:** Unique (Unico).
   - **B:** Binary (Binario).
   - **UN:** Unsigned (No firmado).
   - **ZF:** Zero fill (Relleno de zero).
   - **AI:** Auto increment (Auto incremental).
   - **G:** Generated (Generado).
  
## Ejemplo de Definición de Datos para la Base de Datos "Rallané":

- **Tabla Pasajeros**:
  ```sql
    CREATE TABLE Pasajeros (
        idPasajero INT PRIMARY KEY AUTO_INCREMENT,
        Nombre VARCHAR(100) NOT NULL,
        Documento VARCHAR(45) NOT NULL UNIQUE,
        TipoDocumento VARCHAR(15) NOT NULL,
        FechaNacimiento DATE NOT NULL,
        Nacionalidad VARCHAR(45) NOT NULL
    );
  ```

- **Tabla Vuelos**:
  ```sql
    CREATE TABLE Vuelos (
        IdVuelo INT PRIMARY KEY AUTO_INCREMENT,
        NPlazas INT NOT NULL,
        Origen VARCHAR(45) NOT NULL,
        Destino VARCHAR(45) NOT NULL,
        Compañía VARCHAR(45) NOT NULL,
        FechaHora DATETIME NOT NULL
    );
  ```

- **Tabla Reservas**:
  ```sql
    CREATE TABLE Reservas (
        idReserva INT PRIMARY KEY AUTO_INCREMENT,
        idPasajero INT NOT NULL,
        idVuelo INT NOT NULL,
        Asiento VARCHAR(15) NOT NULL,
        FOREIGN KEY (idPasajero) REFERENCES Pasajeros(idPasajero),
        FOREIGN KEY (idVuelo) REFERENCES Vuelos(IdVuelo)
    );
  ```

## Referencias

[Página w3schools](https://www.w3schools.com/)

[Página mysql](https://dev.mysql.com/doc/refman/8.0/en/innodb-autocommit-commit-rollback.html)