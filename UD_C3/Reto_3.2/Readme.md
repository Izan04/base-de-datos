# Reto 3.2 DCL Control de acceso

Para trabajar con usuarios y privilegios necesitaremos saber una serie de comandos

Este readme lo puedes encontrar en https://gitlab.com/Izan04/base-de-datos.git

## Cómo registrar nuevos usuarios, modificarlos y eliminarlos.

### Registrar Usuarios

#### Crear Usuario:

- Para crear un usuario, necesitaremos usar el 'CREATE', voy a poner de ejemplo el usuario 'paco' con la contraseña 'paco123' y en cualquier ip, utilizando `%`.

```sql
CREATE USER 'paco'@'%' IDENTIFIED BY 'paco123';
```

#### Modificar Usuarios

- Para cambiar el nombre de usuario de algun usuario, usaremos 'rename' de la siguiente forma:

```sql
RENAME USER 'paco'@'%' TO 'paco_Nuevo'@'%'
```

- Puedes cambiar la contraseña del usuario con 'ALTER' de esta forma.

```sql
ALTER USER 'paco'@'%' IDENTIFIED BY 'nueva_contraseña';
```

#### Eliminar Usuarios

- Para eliminar un usuario existente usaremos el comando 'DROP'.

```sql
DROP USER 'paco'@'%'
```

## Cómo se autentican los usuarios y qué opciones de autenticación nos ofrece el SGBD.

### Autentificación basada en contraseña

- Crear un usuario con el plugin de autenticación por defecto (caching_sha2_password)

````sql
CREATE USER 'usuario'@'host' IDENTIFIED BY 'contraseña';
````

- Crear un usuario con mysql_native_password

````sql
CREATE USER 'usuario'@'host' IDENTIFIED WITH 'mysql_native_password' BY 'contraseña';
````

### Autenticación Basada en Plugins:

- Crear un usuario con autenticación PAM

````sql
CREATE USER 'usuario'@'host' IDENTIFIED WITH 'pam' BY 'contraseña';
````
- Crear un usuario con autenticación GSSAPI/Kerberos

````sql
CREATE USER 'usuario'@'host' IDENTIFIED WITH 'gssapi';
````

- Crear un usuario con autenticación LDAP

````sql
CREATE USER 'usuario'@'host' IDENTIFIED WITH 'auth_ldap';
````

- Crear un usuario con autenticación de socket Unix

````sql
CREATE USER 'usuario'@'host' IDENTIFIED WITH 'auth_socket';
````

### Autenticación Basada en Certificados SSL/TLS:

-- Crear un usuario que requiere SSL

````sql
CREATE USER 'usuario'@'host' REQUIRE SSL;
````

-- Crear un usuario que requiere un certificado X.509 válido

````sql
CREATE USER 'usuario'@'host' REQUIRE X509;
````

## Cómo mostrar los usuarios existentes y sus permisos.

- Mostrar usuarios existentes

```sql
SELECT User, Host FROM mysql.user;
```

- Mostrar permisos de un usuario en especifico

```sql
SHOW GRANTS FOR 'usuario'@'host';
```

## Qué permisos puede tener un usuario y qué nivel de granularidad nos ofrece el SGBD.

### Nivel Global:

- Privilegios que se aplican a todas las bases de datos en el servidor MySQL.
Se conceden utilizando:

````sql
GRANT ALL PRIVILEGES ON *.* TO 'usuario'@'host'.
````

### Nivel de Base de Datos:

- Privilegios que se aplican a todas las tablas de una base de datos específica.
Se conceden utilizando:

````sql
GRANT ALL PRIVILEGES ON nombre_base_datos.* TO 'usuario'@'host'.
````

### Nivel de Tabla:

- Privilegios que se aplican a una tabla específica dentro de una base de datos.
Se conceden utilizando:

````sql
GRANT SELECT, INSERT ON nombre_base_datos.nombre_tabla TO 'usuario'@'host'.
````

### Nivel de Columna:

- Privilegios que se aplican a columnas específicas de una tabla específica.
Se conceden utilizando:

````sql
GRANT SELECT(nombre_columna) ON nombre_base_datos.nombre_tabla TO 'usuario'@'host'.
````

### Nivel de Procedimiento/Función:

- Privilegios que se aplican a procedimientos almacenados o funciones específicas.
Se conceden utilizando:

````sql
GRANT EXECUTE ON PROCEDURE nombre_base_datos.nombre_procedimiento TO 'usuario'@'host'.
````

### Tipos de Privilegios

MySQL ofrece una amplia gama de privilegios que pueden ser otorgados a los usuarios. Algunos de los más comunes incluyen:

- ALL PRIVILEGES: Concede todos los privilegios posibles, excepto GRANT OPTION.
- ALTER: Permite modificar la estructura de una tabla.
- CREATE: Permite crear bases de datos y tablas.
- DELETE: Permite eliminar filas de una tabla.
- DROP: Permite eliminar bases de datos y tablas.
- EXECUTE: Permite ejecutar procedimientos almacenados y funciones.
- INDEX: Permite crear y eliminar índices.
- INSERT: Permite insertar filas en una tabla.
- SELECT: Permite leer datos de una tabla.
- UPDATE: Permite actualizar filas en una tabla.
- GRANT OPTION: Permite otorgar a otros usuarios los privilegios que posee el usuario.

### Ejemplos de Concesión de Privilegios

- Conceder Privilegios a Nivel Global:

````sql
GRANT ALL PRIVILEGES ON *.* TO 'usuario'@'host' WITH GRANT OPTION;
````

- Conceder Privilegios a Nivel de Base de Datos:

````sql
GRANT SELECT, INSERT, UPDATE ON nombre_base_datos.* TO 'usuario'@'host';
````

- Conceder Privilegios a Nivel de Tabla:

````sql
GRANT SELECT, INSERT ON nombre_base_datos.nombre_tabla TO 'usuario'@'host';
````

- Conceder Privilegios a Nivel de Columna:

````sql
GRANT SELECT(nombre_columna) ON nombre_base_datos.nombre_tabla TO 'usuario'@'host';
````

- Conceder Privilegios a Nivel de Procedimiento:

````sql
GRANT EXECUTE ON PROCEDURE nombre_base_datos.nombre_procedimiento TO 'usuario'@'host';
````

- Revocar Privilegios:

````sql
REVOKE SELECT, INSERT ON nombre_base_datos.nombre_tabla FROM 'usuario'@'host';
````

## Qué permisos hacen falta para gestionar los usuarios y sus permisos.

### Para gestionar usuarios y sus permisos en MySQL, se necesitan privilegios específicos que permiten la creación, modificación, eliminación y asignación de permisos a los usuarios. Los privilegios necesarios son principalmente:

- CREATE USER: Permite crear nuevas cuentas de usuario.

- DROP USER: Permite eliminar cuentas de usuario existentes.

- ALTER USER: Permite modificar las características de las cuentas de usuario existentes, como cambiar contraseñas o modificar atributos de autenticación.

- GRANT OPTION: Permite otorgar y revocar privilegios a otros usuarios.

- RELOAD: Permite recargar las tablas de privilegios. Esto es útil cuando se han hecho cambios directos en las tablas de privilegios en la base de datos mysql.


## Posibilidad de agrupar los usuarios (grupos/roles/...).

- Crear un Rol:

````sql
CREATE ROLE grupo1;
````

- Otorgar Permisos al Rol:

````sql
GRANT SELECT, INSERT ON base_datos.tabla TO grupo1;
````

- Asignar Usuarios al Rol:

````sql
GRANT grupo1 TO 'usuario1'@'localhost';
GRANT grupo1 TO 'usuario2'@'localhost';
````