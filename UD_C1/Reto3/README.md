# Reto 2: Consultas básicas III

Izan Garrido.

En este reto trabajamos con la base de datos `videoclub` que nos viene dados en el fichero `videoclub.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El codigo fuente de las siguientes consultas se puede consultar en: https://gitlab.com/Izan04/base-de-datos.git


## Query 1 
Para seleccionar El tiulo de la pelicula y su genero seleccionare `P.Titol` y `G.Descripcio` de `PELICULA`, que usare un join de `GENERE` ON `P.CodiGenre = G.CodiGenre`, y lo ordenare con un `ORDER BY` por `P.Titol` en orden ascendente con `ASC`:

```sql
USE videoclub;

SELECT P.Titol AS 'NOMBRE PELICULA',
	G.Descripcio AS 'GENERO'
FROM PELICULA AS P
JOIN GENERE AS G
	ON P.CodiGenere = G.CodiGenere
ORDER BY P.Titol ASC;
```


## Query 2
Para seleccionar todo del cliente, el codigo de factura, la fecha y el importe, seleccionaré `CLIENT.*`, `FACTURA.CodiFactura`, `FACTURA.Data` y `FACTURA.Import`, y haré un join de `FACTURA`, ON `CLIENT.DNI` sea igual a `FACTURA.DNI`, cuando el nombre del cliente `(C.Nom)` sea maría, con `LIKE 'Maria%'`.

```sql
USE videoclub;

SELECT C.*, F.CodiFactura, F.Data, F.Import
FROM CLIENT AS C
JOIN FACTURA AS F
	ON  C.DNI = F.DNI
WHERE C.Nom LIKE 'Maria%';
```


## Query 3
Seleccionaré `PELICULA.Titol`, y `ACTOR.Nom` de `PELICULA`, y haré un join de `ACTOR`, ON `PELICULA.CodiActor` sea igual a `ACTOR.CodiActor`, ordenado con `ORDER BY ACTOR.Nom ASC`.

```sql
USE videoclub;

SELECT P.Titol, A.Nom FROM PELICULA AS P
JOIN ACTOR AS A
	ON P.CodiActor = A.CodiActor
ORDER BY A.Nom ASC;

```


## Query 4
Seleccionaré `PELICULA.Titol`, y `ACTOR.Nom` de `PELICULA`, y haré un join de `INTERPRETADA`, y otro join de `ACTOR` ON `PELICULA.CodiPeli` sea igual a `INTERPRETADA.CodiPeli` y `INTERPRETADA.CodiActor` sea igual a `ACTOR.CodiActor`.

```sql
USE videoclub;

SELECT P.Titol, A.Nom FROM PELICULA AS P
JOIN INTERPRETADA AS I	
	JOIN ACTOR AS A
		ON P.CodiPeli = I.CodiPeli
			AND I.CodiActor = A.CodiActor;
```
