USE videoclub;

SELECT P.Titol AS 'NOMBRE PELICULA',
	G.Descripcio AS 'GENERO'
FROM PELICULA AS P
JOIN GENERE AS G
	ON P.CodiGenere = G.CodiGenere
ORDER BY P.Titol ASC;

-- SELECT P.Titol AS 'NOMBRE PELICULA',
-- 	G.Descripcio AS 'GENERO'
-- FROM PELICULA AS P, GENERE AS G
-- WHERE P.CodiGenere = G.CodiGenere
-- ORDER BY P.Titol ASC;