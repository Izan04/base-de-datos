USE instagram_low_cost;
SELECT u.idUsuario, u.nombre AS nombreUsuario, COUNT(rc.idTipoReaccion) AS totalMeGusta
	FROM usuarios u
	JOIN roles r ON u.idRol = r.idRol
		JOIN reaccionesComentarios rc ON u.idUsuario = rc.idUsuario
			JOIN tiposReaccion tr ON rc.idTipoReaccion = tr.idTipoReaccion
			WHERE r.descripcion = 'Administrador' AND tr.descripcion = 'Me gusta'
				GROUP BY u.idUsuario
				HAVING totalMeGusta > 2;