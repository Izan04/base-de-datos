# Reto 2: Consultas básicas II

Izan Garrido.

En este reto trabajamos con las bases de datos `instagramlowcost`, que nos vienen dada en el fichero `instragramlowcost2.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El codigo fuente de las siguientes consultas se puede consultar en: https://gitlab.com/Izan04/base-de-datos.git
En el apartado UD_C1.


## Query 1
Fotos del usuario con ID 36

```sql
USE instagram_low_cost;
 SELECT descripcion AS 'fotos'
	FROM fotos
		WHERE idusuario = 36;
```


## Query 2
Fotos del usuario con ID 36 tomadas en enero del 2023

```sql
 USE instagram_low_cost;
 SELECT descripcion AS fotos
	FROM fotos
		WHERE idusuario = 36
        AND fechaCreacion LIKE "2023-01%";
```


## Query 3
Comentarios del usuario 36 sobre la foto 12 del usuario 11

```sql
USE instagram_low_cost;
SELECT c.comentario
FROM fotos f
JOIN comentariosFotos cf 
ON f.idFoto = cf.idFoto
JOIN Comentarios c
ON cf.idComentario = c.idComentario
WHERE c.idUsuario = 36
AND f.idFoto = 12
```	


## Query 4
Fotos que han sorprendido al usuario 25

```sql
USE instagram_low_cost;

SELECT f.idfoto, f.descripcion
	FROM fotos f, reaccionesFotos rf
		WHERE rf.idTipoReaccion = 4
        AND f.idUsuario = 25;
```

## Query 5
Administradores que han dado más de  2 “Me gusta”.

```sql
USE instagram_low_cost;
SELECT u.idUsuario, u.nombre AS nombreUsuario, COUNT(rc.idTipoReaccion) AS totalMeGusta
	FROM usuarios u
	JOIN roles r ON u.idRol = r.idRol
		JOIN reaccionesComentarios rc ON u.idUsuario = rc.idUsuario
			JOIN tiposReaccion tr ON rc.idTipoReaccion = tr.idTipoReaccion
			WHERE r.descripcion = 'Administrador' AND tr.descripcion = 'Me gusta'
				GROUP BY u.idUsuario
				HAVING totalMeGusta > 2;
```


## Query 6
Número de “Me divierte” de la foto número 12 del usuario 45

```sql
USE instagram_low_cost;
SELECT COUNT(*) AS totalMeDivierte
	FROM reaccionesFotos rf
		JOIN tiposReaccion tr ON rf.idTipoReaccion = tr.idTipoReaccion
		WHERE rf.idUsuario = 45
			AND rf.idFoto = 12
			AND tr.descripcion = 'Me divierte';
```

## Query 7
Número de fotos tomadas en la playa (en base al título)

```sql
USE instagram_low_cost;
SELECT COUNT(*) AS totalFotosEnLaPlaya
	FROM fotos
		WHERE descripcion LIKE '%playa%';
```