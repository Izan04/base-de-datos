# Reto 1: Consultas básicas

Izan Garrido.

En este reto trabajamos con la base de datos `sanitat`, que nos viene dada en el fichero `sanitat.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en:
https://gitlab.com/Izan04/base-de-datos.git

## Query 1
Para seleccionar el número, nombre y teléfono de todos los hospitales existentes, seleccionaremos estos tres atributos, que se corresponden con las columnas `HOSPITAL_COD`, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT HOSPITAL_COD AS 'CODIGO HOSPITAL', NOM, TELEFON
FROM HOSPITAL;
```


## Query 2
Para seleccionar el número, nombre y teléfono que tengan una letra A en la segunda posición del nombre, seleccionaremos estos tres atributos, que se corresponden con las columnas `HOSPITAL_COD`, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL`, además lo seleccionaremos solo cuando la segunda letra de `NOM` sea una `a`, Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT HOSPITAL_COD AS 'CODIGO HOSPITAL', NOM, TELEFON
FROM HOSPITAL 
WHERE NOM LIKE "_a%";   
```


## Query 3
Para seleccionar el código hospital, código sala, número empleado y
apellido existentes, seleccionaremos estos cuatro atributos, que se corresponden con las columas `HOSPITAL_COD`, `SALA_COD`, `EMPLEAT_NO` y `COGNOM`, respectivamente de la tabla `PLANTILLA`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT HOSPITAL_COD AS 'CODIGO HOSPITAL' , SALA_COD AS 'CODIGO SALA', EMPLEAT_NO AS 'NUM EMPLEADO', COGNOM
FROM PLANTILLA;
```


## Query 4
Para seleccionar el código hospital, código sala, número empleado y
apellido que no sean del turno de noche, seleccionaremos estos cuatro atributos , que se corresponden con las columas `HOSPITAL_COD`, `SALA_COD`, `EMPLEAT_NO` y `COGNOM`, respectivamente de la tabla `PLANTILLA`.  y lo seleccionaremos solo cuando `TORN` no sea igual a `N`, lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT HOSPITAL_COD AS 'CODIGO HOSPITAL' , SALA_COD AS 'CODIGO SALA', EMPLEAT_NO AS 'NUM EMPLEADO', COGNOM
FROM PLANTILLA
WHERE TORN != 'N';
```

## Query 5
Para mostrar a los enfermos nacidos en 1960 seleccionaremos el nombre de los enfermeros que se corresponde al atributo `COGNOM`, respectivamente de la tabla `MALALT` y seleccionaremos solo los casos en los que `DATA_NAIX`, que se refiere al año de nacimiento, sea igual a `1960`, lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT COGNOM
FROM MALALT
WHERE YEAR(DATA_NAIX) = '1960';
```


## Query 6
Para mostrar a los enfermos nacidos en 1960 seleccionaremos el nombre de los enfermeros que se corresponde al atributo `COGNOM`, respectivamente de la tabla `MALALT` y seleccionaremos solo los casos en los que `DATA_NAIX`, que se refiere al año de nacimiento, sea igual o superior a `1960`, lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT COGNOM
FROM MALALT
WHERE YEAR(DATA_NAIX) >= '1960';
```
