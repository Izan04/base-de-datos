# Reto 2: Consultas básicas II

Izan Garrido.

En este reto trabajamos con las bases de datos `empresa`, y `videoclub` que nos vienen dados en los ficheros `empresa.sql` y `videoclub.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El codigo fuente de las siguientes consultas se puede consultar en: https://gitlab.com/Izan04/base-de-datos.git


## Query 1
Para seleccionar el código y descripción que comercializa la empresa, seleccionare estos 2 atributos, que se corresponden con las columnas `PROD_NUM` y `DESCRIPCIO`, respectivamente de la tabla `HOSPITAL`:  

```sql
USE empresa;

SELECT PROD_NUM AS 'codigo',
	DESCRIPCIO AS 'descripción'
FROM PRODUCTE;
```


## Query 2
Para cumplir con el segundo enunciado y mostrar los productos (código y descripción) que contienen la palabra "tenis" en la descripción, aplicamos un filtro con LIKE en la columna DESCRIPCIO de la tabla PRODUCTE:

```sql
USE empresa;

SELECT PROD_NUM AS 'codigo',
	DESCRIPCIO AS 'descripción'
FROM PRODUCTE
WHERE DESCRIPCIO LIKE "%TENNIS%";
```


## Query 3
Para cumplir con el tercer enunciado y mostrar el código, nombre, área y teléfono de los clientes de la empresa, seleccionamos los atributos CLIENT_COD, NOM, AREA, y TELEFON respectivamente de la tabla CLIENT:

```sql
USE empresa;

SELECT CLIENT_COD AS 'codigo',
	NOM AS 'nombre',
	AREA AS 'àrea',
	TELEFON AS 'telefono'
FROM CLIENT;
```


## Query 4
Para cumplir con el cuarto enunciado y mostrar los clientes (código, nombre, ciudad) que no son del área telefónica 636, aplicamos un filtro WHERE NOT LIKE en la columna AREA de la tabla CLIENT:

```sql
USE empresa;

SELECT CLIENT_COD AS 'codigo',
	NOM AS 'nombre',
	AREA AS 'àrea',
	TELEFON AS 'telefono'
FROM CLIENT
WHERE AREA NOT LIKE "636";
```

## Query 5
Para cumplir con el quinto enunciado y mostrar las órdenes de compra de la tabla de pedidos (código, fechas de orden y de envío), seleccionamos los atributos CLIENT_COD, COM_DATA y DATA_TRAMESA respectivamente de la tabla COMANDA:

```sql
USE empresa;

SELECT CLIENT_COD AS 'código',
	COM_DATA AS 'fecha de orden',
	DATA_TRAMESA AS 'fecha envío'
FROM COMANDA;
```


## Query 6
Para cumplir con el sexto enunciado y mostrar la lista de nombres y teléfonos de los clientes del videoclub, seleccionamos los atributos Nom y Telefon respectivamente de la tabla CLIENT:

```sql
USE videoclub;

SELECT Nom AS 'nombre',
	Telefon AS 'telefono'
FROM CLIENT;
```

## Query 7
Para cumplir con el séptimo enunciado y mostrar la lista de fechas e importes de las facturas del videoclub, seleccionamos los atributos Data e Import respectivamente de la tabla FACTURA:

```sql
USE videoclub;

SELECT Data AS 'fecha',
	Import AS 'importe'
FROM FACTURA;
```

## Query 8
Para cumplir con el octavo enunciado y mostrar la lista de productos (descripción) facturados en la factura número 3, aplicamos un filtro WHERE para seleccionar solo los registros con el código de factura igual a 3 en la tabla DETALLFACTURA:

```sql
USE videoclub;

SELECT Descripcio AS 'producto',
	CodiFactura AS 'factura'
FROM DETALLFACTURA
WHERE CodiFactura = 3;
```

## Query 9
Para cumplir con el noveno enunciado y mostrar la lista de facturas ordenada de forma decreciente por importe, utilizamos la cláusula ORDER BY con la columna Import y la palabra clave desc en la tabla FACTURA:

```sql
USE videoclub;

SELECT *
FROM FACTURA
ORDER BY Import desc;
```

## Query 10
Para cumplir con el décimo enunciado y mostrar la lista de los actores cuyo nombre comience por "X", utilizamos el operador LIKE con el patrón "X%" en la columna Nom de la tabla ACTOR:

```sql
USE videoclub;

SELECT *
FROM ACTOR
WHERE Nom LIKE "X%";
```