USE empresa;

SELECT CLIENT_COD AS 'codigo',
	NOM AS 'nombre',
	AREA AS 'àrea',
	TELEFON AS 'telefono'
FROM CLIENT
WHERE AREA NOT LIKE "636";